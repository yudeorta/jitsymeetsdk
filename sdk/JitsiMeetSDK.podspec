Pod::Spec.new do |s|
  s.name             = 'JitsiMeetSDK'
  s.version          = '2.4.0'
  s.summary          = 'Jitsi Meet iOS SDK'
  s.description      = 'Jitsi Meet is a WebRTC compatible, free and Open Source video conferencing system that provides browsers and mobile applications with Real Time Communications capabilities.'
  s.homepage         = 'https://bitbucket.org/yudeorta/jitsymeetsdk'
  s.license          = 'Apache 2'
  s.authors          = 'The Jitsi Meet project authors'
  s.source           = { :git => 'https://bitbucket.org/yudeorta/jitsymeetsdk/jitsymeetsdk.git' }

  s.platform         = :ios, '10.0'

  s.vendored_frameworks = 'Frameworks/JitsiMeet.framework', 'Frameworks/WebRTC.framework'
end
